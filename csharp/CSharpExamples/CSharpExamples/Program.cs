using System;

namespace CSharpExamples
{
	class RunSimpleExamples
	{
		public static void Main (string[] args)
		{
			ExplicitSimpleLambda esl = new ExplicitSimpleLambda ();
			esl.RunExample ();

			PredicateSimpleLambda psl = new PredicateSimpleLambda ();
			psl.RunExample ();
		}
	}
}
