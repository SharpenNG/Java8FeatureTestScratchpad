using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpExamples
{
	public class PredicateSimpleLambda
	{
		public PredicateSimpleLambda ()
		{

		}

		public void RunExample() {
			double[] numbers = {-1, 1, -2, 2, -3, 3};
			String s = PrintNumbers(numbers, (double d) => d > 0);
			Console.WriteLine(s);
		}

		public String PrintNumbers(double[] numbers, Predicate<double> test) {
			List<double> filtered = new List<double>();

			foreach(double d in numbers) {
				if(test(d)) {
					filtered.Add (d);
				}
			}

			filtered.Sort();

			StringBuilder sb = new StringBuilder ();

			foreach (double d in filtered) {
				sb.Append(d + " ");
			}

			return sb.ToString().Trim();
		}

	}
}

