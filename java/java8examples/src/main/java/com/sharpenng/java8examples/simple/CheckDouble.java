package com.sharpenng.java8examples.simple;

public interface CheckDouble {
	boolean test(double d);
}
