package com.sharpenng.java8examples.simple;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public class PredicateSimpleLambda {

	public PredicateSimpleLambda() {
		
	}
	
	public void runExample() {
		double[] numbers = new double[]{-1, 1, -2, 2, -3, 3};
		String s = printNumbers(numbers, (Double d) -> d > 0);
		System.out.println(s);
	}
	
	public String printNumbers(double[] numbers, Predicate<Double> check) {
		List<Double> filtered = new ArrayList<Double>();
		
		for(double d : numbers) {
			if(check.test(d)) {
				filtered.add(d);
			}
		}
		
		Collections.sort(filtered);
		
		StringBuffer sb = new StringBuffer();
		for(double d : filtered) {
			sb.append(d + " ");
		}
		
		return sb.toString().trim();
	}

}
