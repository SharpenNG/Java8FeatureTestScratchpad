package com.sharpenng.java8examples.simple;

public class RunSimpleExamples {

	public static void main(String[] args) {
		ExplicitSimpleLambda esl = new ExplicitSimpleLambda();
		esl.runExample();
		
		PredicateSimpleLambda psl = new PredicateSimpleLambda();
		psl.runExample();

	}

}
